#include "lab2.h"

using namespace std;

// главная функция лабораторной
void Lab2_main()
{
    string str;
    ifstream infile("C:\\Qt\\Projects\\Lab11\\lab2.cpp");
    char c;
    while(infile.get(c))
        str+=c;
    formatString(str);
    parseString(str);
    cout << str << endl;
}

// форматирования исходной строки по задани.
void formatString(string &str)
{
    unifySpaces(str);
    deleteMultipleSpaces(str);
    modifyMarkSpaces(str);
    modifyLongWords(str);
}

// изменение длинных строк на другое
void modifyLongWords(string &str)
{
    size_t prev = NULL;
    size_t found = str.find(' ');
    string insertableToken = "Vau!!!"; // вставляемое слово
    while(found != string::npos)
    {
        if((found-prev)>10)
        {
            str.replace(prev, found-prev, insertableToken);
            prev = prev + insertableToken.length();
        }
        else
            prev = found + 1;
        found = str.find(' ', prev);
    }
}

// удаление повторяющихся пробелов
void deleteMultipleSpaces(string &str)
{
    size_t found = str.find("  ");
    while(found != string::npos)
    {
        str.erase(found, 1);
        found = str.find("  ",found);
    }
}

// удаление пробелов межд словом и знаком и выставление после знака
void modifyMarkSpaces(string &str)
{
    size_t found = str.find_first_of("\.\,\!\?\:\;");
    while(found != string::npos)
    {
        if((str[found-1]==' ')&&(isalnum(str[found-2])))
        {
            str.erase(found-1, 1);
            --found;
        }
        if (str[found+1]!=' ')
            str.insert(found+1, 1,' ');
        found = str.find_first_of("\.\,\!\?\:\;", found+1);

    }
}

// преобразование табов и переносов в пробелы
void unifySpaces(string &str)
{
    size_t found = str.find_first_of("\n\t");
    while(found != string::npos)
    {
        str[found] = ' ';
        found = str.find_first_of("\n\t", found+1);
    }
}

// расстановка переносов на 40-м символе
void parseString(string &str)
{
    for(int i=0, r = i+40; r<str.size(); r = i+40)
    {
        while(str[r]!=' ')
            --r;
        str.insert(r+1,"\n");
        i=r;
    }
}
