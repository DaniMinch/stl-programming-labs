/*1. Напишите программу – «телефонную книжку».
Записи(имя и телефон) должны хранится в каком - либо STL - контейнере(vector или list), причем крайне желательно, чтобы от типа контейнера не зависело ничего, кроме одной строки в программе – объявления этого контейнера(указание: используйте typedef).

Программа должна поддерживать следующие операции :
Просмотр текущей записи
Переход к следующей записи
Переход к предыдущей записи
Вставка записи перед / после просматриваемой
Замена просматриваемой записи
Вставка записи в конец базы данных
Переход вперед / назад через n записей.*/
#ifndef LAB4_H
#define LAB4_H

#include <iostream>
#include <Phonebook.h>

enum class CMD
{
    CURRENT,
    NEXT,
    PREVIOUS,
    MOVE,
    INSERT_AFTER,
    INSERT_BEFORE,
    INSERT_END,
    REPLACE,
    SHOW_ALL,
    HELP
};

void Lab4_main();
void drawList(const Phonebook& phoneBook);
void drawHelp();
void commandsHandler(Phonebook &pb, CMD cmd);


#endif // LAB4_H
