/*1.	Написать программу, которая выполняет следующие действия:
a.	Читает содержимое текстового файла
b.	Выделяет слова, словом считаются последовательность символов разделенных
пробелами и/или знаками табуляции и/или символами новой строки
c.	Вывести список слов присутствующий в тексте без повторений (имеется в виду,
что одно и то же слово может присутствовать в списке только один раз)

2.	Написать программу, которая выполняет следующие действия (все операции
должны выполняться с помощью стандартных алгоритмов):
a.	Заполняет вектор геометрическими фигурами. Геометрическая фигура может
быть треугольником, квадратом, прямоугольником или пяти угольником. Структура
описывающая геометрическую фигуру  определена ниже,
b.	Подсчитывает общее количество вершин всех фигур содержащихся в векторе
(так треугольник добавляет к общему числу 3, квадрат 4 и т.д.)
c.	Подсчитывает количество треугольников, квадратов и прямоугольников в векторе
d.	Удаляет все пятиугольники
e.	На основании этого вектора создает vector<Point>, который содержит
координаты одной из вершин (любой) каждой фигуры, т.е. первый элемент этого
вектора содержит координаты одной из вершину первой фигуры, второй элемент
этого вектора содержит координаты одной из вершину второй фигуры и т.д.
f.	Изменяет вектор так, чтобы он содержал в начале все треугольники, потом все
квадраты, а потом прямоугольники.
g.	Распечатывает вектор после каждого этапа работы

*/
#ifndef LAB6_H
#define LAB6_H

#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>


void Lab6_main();
std::vector<std::string> parseStringToVector(std::string& str);
void showUniqueWords(std::vector<std::string> &);

struct Point
{
    double x,y;
};

struct Shape
{
    Shape(int _num, bool _isSquare);
    Shape(){}            // Undefined Behavior Нужно для работы алгоритмов
    int vertex_num;      // количество вершин, для треугольника 3, для квадрата
                         // и прямоугольника 4, для пятиугольника 5
    std::vector<Point> vertexes;     // вектор содержащий координаты вершин фигуры
                                // Для треугольника содержит 3 элемента
                                // Для квадрата и прямоугольника содержит 4
                                // элемента
                                // Для пятиугольника 5 элементов
    bool square;              // можно проверять через координаты, но это
                                // ОЧЕНЬ долго
} ;

// структура для подсчета количества вершин
// operator() вызывается в for_each, число записывается в  counter
struct countVertex {
public:
    countVertex():counter(0){}
  void operator() (Shape sh){counter+=sh.vertex_num;}
  const int& getCounter(){return counter;}
private:
  int counter;
};


std::vector<Shape> createVector(int length);
std::vector<Point> createPointVectorfromShapes(const std::vector<Shape>&);
Shape createRandomShape();
Point getRandomPoint(const Shape&);
void countShapes(const std::vector<Shape>&);
bool isTriangle(const Shape& sh);
bool isSquare(const Shape& sh);
bool isRectangle(const Shape& sh);
bool isPentagon(const Shape& sh);
void printPoint(Point);
void printShape(Shape);
bool sortShapes(Shape,Shape);



#endif // LAB6_H
