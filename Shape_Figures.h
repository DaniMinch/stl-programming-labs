#include "Shape.h"

class Circle :
    public Shape_8
{
public:
    Circle(int x, int y) : Shape_8(x, y) { }
    //~Circle() { }

    virtual void draw() const override
    {
        std::cout << "Circle(" << center.x << ", " << center.y << ")";
    }
};

class Triangle :
    public Shape_8
{
public:
    Triangle(int x, int y) : Shape_8(x, y) { }
    //~Triangle() { }

    virtual void draw() const override
    {
        std::cout << "Triangle(" << center.x << ", " << center.y << ")";
    }
};

class Square :
    public Shape_8
{
public:
    Square(int x, int y) : Shape_8(x, y) { }
    //~Square() { }

    virtual void draw() const override
    {
        std::cout << "Square(" << center.x << ", " << center.y << ")";
    }
};

