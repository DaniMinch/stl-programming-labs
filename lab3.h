/*1.	Ниже приведен интерфейс класса очереди с приоритетами, который
 * функционирует следующим образом.

В очередь могут быть добавлены элементы, каждому элементу при добавлении
присваивается один из трех уровней приоритета (low, normal, high)

Элементы из очереди извлекаются в соответствии с их приоритетами (сначала
извлекаются элементы с приоритетом high, потом normal, потом low), элементы
с одинаковыми приоритетами извлекаются из очереди в порядки их поступления.

В очереди также может происходить операция акселерации – все элементы с
приоритетом low находящиеся в момент акселерации в очереди увеличивают свой
приоритет до high и «обгоняют» элементы с приоритетом normal.

2.  Разработайте программу, которая
a.	Заполняет list<int> 15 случайными значениями от 1 до 20, список может
содержать от 0 до 20 значений (обязательно проверить на длине списка 0, 1. 2,
3, 4, 5, 7, 14)
b.	Выводит содержимое списка в следующем порядке: первый элемент, последний
элемент, второй элемент, предпоследний элемент, третий элемент и т.д.
*/
#ifndef LAB3_H
#define LAB3_H

#include <iostream>
#include <list>
#include <deque>
#include <ctime>
#include <string.h>

void Lab3_main();
std::list<int> createrandomList();
void showList(std::list<int> lst);

typedef enum
{
    LOW,
    NORMAL,
    HIGH
} ElementPriority;

struct QueueElement
{
public:
    std::string name;
    QueueElement(std::string _name = ""):name(_name){}
    friend std::ostream& operator<<(std::ostream& s, QueueElement& t){return s<<t.name;}
};

class QueueWithPriority
{
public:
    // Конструктор, создает пустую очередь
    QueueWithPriority();

    // Деструктор
    ~QueueWithPriority();

    // Добавить в очередь элемент element с приоритетом priority
    void PutElementToQueue(const QueueElement& element, ElementPriority priority);

    // Получить элемент из очереди
    // метод должен возвращать элемент с наибольшим приоритетом, который был
    // добавлен в очередь раньше других
    QueueElement GetElementFromQueue();

    // Выполнить акселерацию
    void Accelerate();

    // вывести очередь на экран
    void printQueue();

private:
    // очереди по приоритетам
    std::deque<QueueElement>* deqH;
    std::deque<QueueElement>* deqN;
    std::deque<QueueElement>* deqL;
    int counter[3];
};


#endif // LAB3_H
