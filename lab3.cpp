#include "lab3.h"

using namespace std;

void Lab3_main()
{
    srand(time(NULL));
    list<int> lst = createrandomList();
    showList(lst);

    QueueWithPriority* que = new QueueWithPriority();
    que->PutElementToQueue(QueueElement("tratata"),HIGH);
    que->PutElementToQueue(QueueElement("123"),NORMAL);
    que->PutElementToQueue(QueueElement("456"),LOW);
    que->PutElementToQueue(QueueElement("789"),HIGH);
    que->printQueue();
    que->Accelerate();
    que->printQueue();
    QueueElement my = que->GetElementFromQueue();
    cout<<my<<endl;
    que->printQueue();
}

// создает список, забитый центральными защитниками
list<int> createrandomList()
{
    list<int> lst;
    for(int i=0; i<15;++i)
        lst.push_back(1+rand()%20); //1-20
    for(auto it=lst.begin(); it!=lst.end(); ++it)
        cout<<*it<<"  ";
    return lst;
}

// выводит элементы списка, способом описанным в задании
void showList(list<int> lst)
{
    int jumper = 2;
    cout<<endl<<"Mixed line:"<<endl;
    while(!lst.empty())
    {
        if( jumper%2==0 )
        {
            cout<<lst.front()<<"  ";
            lst.pop_front();
        }
        else
        {
            cout<<lst.back()<<"  ";
            lst.pop_back();
        };
        ++jumper;
    }
    cout<<endl;
}


//---------------------------------------------------------------------

// конструктор создает 3 очереди, обнуляет массив
QueueWithPriority::QueueWithPriority()
{
    deqH = new deque<QueueElement>;
    deqN = new deque<QueueElement>;
    deqL = new deque<QueueElement>;
    memset(counter, 0, sizeof(counter));
}

// бросить элемент в конец одной из очередей
void QueueWithPriority::PutElementToQueue(const QueueElement &element, ElementPriority priority)
{
    switch (priority) {
    case HIGH:
        deqH->push_back(element);
        ++counter[2];
        break;
    case NORMAL:
        deqN->push_back(element);
        ++counter[1];
        break;
    case LOW:
        deqL->push_back(element);
        ++counter[0];
        break;
    default:
        cout<<"ERROR: Wrong priority statement"<<endl;
        break;
    }
}

// выбросить самый высокооприоритетный элемент в очереди (FIFO)
QueueElement QueueWithPriority::GetElementFromQueue()
{
    QueueElement tmp;
    if(!deqH->empty())
    {
        tmp = deqH->front();
        deqH->pop_front();
    }
    else if (!deqN->empty())
    {
        tmp = deqN->front();
        deqN->pop_front();
    }
    else if(!deqL->empty())
    {
        tmp = deqL->front();
        deqL->pop_front();
    }
    else
    {
        cout<<"Error: Queue is empty"<<endl;
    }
    return tmp;
}

// записать элементы с низким приоритетом в конец очереди с высшим,
// очистить очередь LOW
void QueueWithPriority::Accelerate()
{
    for(auto it=deqL->begin();it!=deqL->end();++it)
        deqH->push_back(*it);
    deqL->clear();
}

// Вывод на печать номера эл-та в очереди, значения и приоритета эл-та
void QueueWithPriority::printQueue()
{
    cout<<"All elements of Queue With Priority"<<endl;
    int i=0;
    for(auto it=deqH->begin();it!=deqH->end();++it)
    {
        ++i;
        cout<<i<<".\t"<<*it<<"\tHIGH"<<endl;
    }
    for(auto it=deqN->begin();it!=deqN->end();++it)
    {
        ++i;
        cout<<i<<".\t"<<*it<<"\tNORMAL"<<endl;
    }
    for(auto it=deqL->begin();it!=deqL->end();++it)
    {
        ++i;
        cout<<i<<".\t"<<*it<<"\tLOW"<<endl;
    }
}
