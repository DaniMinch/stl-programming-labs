#include "lab4.h"

using namespace std;

void Lab4_main()
{
    Phonebook pbook;
    pbook.insertLast(new Record("ABC", "123"));
    pbook.insertLast(new Record("DEF", "465"));
    pbook.insertLast(new Record("XYZ", "789"));

    drawHelp();
    int cmd;
    do
    {
        drawList(pbook);
        cout<<"Type a command: ";
        cin >> cmd;
        cout<<endl;
        commandsHandler(pbook, static_cast<CMD>(cmd));
    } while (true);

}

// вывод всей книжки
void drawList(const Phonebook &phoneBook)
{
    cout<< endl;
    for (const auto& rec : phoneBook)
    {
        if (rec == phoneBook.current())
            cout << ">";
        else
            cout << " ";
        cout << *rec << endl;
    }

    cout<<endl<<"----------------------------------------------------"<<endl<<endl;
}

// вывод справки
void drawHelp()
{
    const char lines[][70] =
    {
        { "0 - Show Current Record" },
        { "1 - Next Record" },
        { "2 - Previous Record" },
        { "3 - Move Cursor Ahead On: \"relative position\"" },
        { "4 - Insert Record After Current: \"name, phone\"" },
        { "5 - Insert Record Before Current: \"name, phone\"" },
        { "6 - Insert Record In The End: \"name, phone\"" },
        { "7 - Replace Current: \"name, phone\"" },
        { "8 - Show All Records" },
        { "9 - Show Help" },
        { "----------------------------------------------------" }
    };

    for (const auto str : lines)
    {
        cout << str<<endl;
    }
}

// обработчик команд, принимающий int значения
void commandsHandler(Phonebook &pb, CMD cmd)
{
    string name, phone;
    function<void(Phonebook&, Record*)> callFunc = nullptr;

    switch (cmd)
    {
    case CMD::CURRENT:
        if (!pb.isEmpty())
            cout<<*pb.current()<<endl;
        break;

    case CMD::NEXT:
        if (pb.hasNext())
            pb.moveNext();
        break;

    case CMD::PREVIOUS:
        if (pb.hasPrevious())
            pb.movePrevious();
        break;

    case CMD::MOVE:
    {
        int movPos;
        cin >> movPos;
        pb.move(movPos);
    }
        break;

    case CMD::INSERT_AFTER:
        callFunc = &Phonebook::insertAfter;
        break;

    case CMD::INSERT_BEFORE:
        callFunc = &Phonebook::insertBefore;
        break;

    case CMD::INSERT_END:
        callFunc = &Phonebook::insertLast;
        break;

    case CMD::REPLACE:
        callFunc = &Phonebook::replaceCurrent;
        break;
    case CMD::SHOW_ALL:
        break;
    case CMD::HELP:
        drawHelp();
        break;
    default:
        cout << "Unknown command" << endl;
        break;
    }

    // вызов функции для замены; имя передается через callFunc
    if (callFunc != nullptr)
    {
        cin >> name >> phone;
        callFunc(pb, new Record(name, phone));
    }
}
