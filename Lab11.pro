QT += core
QT -= gui

TARGET = Lab11
CONFIG += c++14 console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    lab2.cpp \
    lab3.cpp \
    lab6.cpp \
    lab7.cpp \
    lab4.cpp \
    lab5.cpp \
    lab8.cpp

DISTFILES += \
    test.txt \
    Lab2_test.txt \
    lab6_test.txt

HEADERS += \
    lab1.h \
    lab2.h \
    lab3.h \
    lab6.h \
    lab7.h \
    Phonebook.h \
    Record.h \
    lab4.h \
    lab5.h \
    lab8.h \
    Shape.h \
    Shape_Figures.h

