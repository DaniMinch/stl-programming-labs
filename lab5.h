#ifndef LAB5_H
#define LAB5_H
#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include <chrono>

#define Arr_DataStruct_Size 10


struct DataStruct
{
    int key1;
    int key2;
    std::string str;

    DataStruct() {}

    DataStruct(int _key1, int _key2, const std::string& _str):
        key1(_key1), key2(_key2), str(_str){}

    friend std::ostream& operator<<(std::ostream& os, const DataStruct& ds)
    {
        os << ds.key1 << " \t " << ds.key2 << " \t " << ds.str;
        return os;
    }
};


const char strings[][10] =
{
    { "1  qwerty"},
    { "2  asdfgh"},
    { "3  12345"},
    { "4  67890" },
    { "5  -+*/" },
    { "6  ----" },
    { "7  eax" },
    { "8  11" },
    { "9  ddd" },
    { "10" },
    { "11 pppp" }
};

std::vector<DataStruct> createDataStructVector(int length =Arr_DataStruct_Size);

template<typename T>
void printVector(const std::vector<T>&);

bool sortVector(const DataStruct&, const DataStruct&);

void Lab5_main();


#endif // LAB5_H
