/*Задание к лабораторной работе. Необходимо выполнить ВСЕ задания;

1)	Напишите алгоритм сортировки (любой простейший) содержимого вектора целых
чисел, используя оператор operator[];

2)	Напишите алгоритм сортировки (любой простейший) содержимого вектора целых
чисел, используя метод at();

3)	Напишите алгоритм сортировки (любой простейший) содержимого вектора целых
чисел, используя для доступа к содержимому вектора только итераторы. Для работы
с итераторами допустимо использовать только операторы получения текущего
элемента и перехода в следующему (подсказка, можно сохранять копию итератора,
указывающего на некоторый элемент);

4)	Прочитайте во встроенный массив С содержимое текстового файлы, скопируйте
данные в вектор одной строкой кода (без циклов и алгоритмов STL;

5)	Напишите программу, сохраняющую в векторе числа, полученные из стандартного
ввода (окончанием ввода является число 0). Удалите все элементы, которые
делятся на 2 (не используете стандартные алгоритмы STL), если последнее число 1.
Если последнее число 2, добавьте после каждого числа которое делится на 3 три
единицы:

6)	Напишите функцию void fillRandom(double* array, int size) заполняющую массив
случайными значениями в интервале от -1.0 до 1.0. Заполните с помощью заданной
функции вектора размером 5,10,25,50,100 и отсортируйте его содержимое (с помощью
любого разработанного ранее алгоритма модифицированного для сортировки
действительных чисел).
*/
#ifndef LAB1
#define LAB1

#include <vector>
#include <iostream>
#include <fstream>
#include <ctime>


#define Arr_Size_Default 25
#define Arr_Size 10000
#define Arr_Size1 5
#define Arr_Size2 25
#define Arr_Size3 50
#define Arr_Size4 100

using namespace std;

// пузырек через []
void bubblesort(vector<int> v = vector<int>())
{
    if(v.empty())
    {
        v.resize(Arr_Size_Default);
        for(int i=0; i<Arr_Size_Default; ++i)
            v[i]=rand();
    }
    for(int i=0; i<v.size(); ++i)
        for(int j=i+1; j<v.size(); ++j)
            if(v[i]>v[j])
            {
                int temp=v[i];
                v[i]=v[j];
                v[j]=temp;
            }
    cout <<"'[]' sorting:\n";
    for(int i=0; i<10; ++i)
        cout<<v[i]<<endl;
}

// пузырек через at
void bubblesortAt(vector<int> v = vector<int>())
{
    if(v.empty())
    {
        v.resize(Arr_Size_Default);
        for(int i=0; i<v.size(); ++i)
            v.at(i)=rand();
    }
    for(int i=0; i<v.size(); ++i)
        for(int j=i+1;j<v.size(); ++j)
            if(v.at(i)>v.at(j))
            {
                int temp=v.at(i);
                v.at(i)=v.at(j);
                v.at(j)=temp;
            }
    cout <<"'At()' sorting:\n";
    for(int i=0; i<10; ++i)
        cout<<v.at(i)<<endl;
}

// пузырек через итератор
void bubblesortIt(vector<int> v = vector<int>())
{
    if(v.empty())
    {
        v.resize(Arr_Size_Default);
        for(vector<int>::iterator it=v.begin(); it!=v.end(); ++it)
            *it = rand();
    }
    int temp = NULL;
    for(vector<int>::iterator it=v.begin(); it!=v.end(); ++it)
        for(vector<int>::iterator it2=it; it2!=v.end(); ++it2)
            if(*it>*it2)
            {
                temp=*it;
                *it=*it2;
                *it2=temp;
            }
    cout <<"Iterator sorting:\n";
    for(int i=0; i<10; ++i)
        cout<<v.at(i)<<endl;
}

// копированние данных из файла в массив.
void copyData()
{
    int i=-1;
    char carr[Arr_Size_Default];
    ifstream infile("C:\\Qt\\Projects\\Lab11\\test.txt",ios_base::in);
    if(infile.is_open())
    {
        char ch;
        while (infile.get(ch))
        {
            ++i;
            carr[i]=ch;
        }
        vector<char> v(carr,carr+i);

        for(i=0; i<v.size(); ++i)
            cout<<v[i];
    }
    else cout << "Error opening file: "<< strerror(errno) << endl;
}

// преобразовывает вектор по заданию 5
void vectorConverter(vector<int> v = vector<int>())
{
    int digit;
    cout<<"Enter digit"<<endl;
    do
    {
        cin>>digit;
        if(digit!=0) //ввод до 0
            v.push_back(digit);
    }
    while(digit!=0);
    if(v.back()==1)
    {
        vector<int>::iterator it = v.begin();
        while(it != v.end())
        {
            if(*it%2==0)    //удаение кратных 2
                it = v.erase(it);
            else
                ++it;
        }
    }
    else
        if(v.back()==2)
            for(vector<int>::iterator it = v.begin();it != v.end();++it)
                if(*it%3 == 0) //три 1 после кратных 3
                {
                    it = v.insert(it+1,1);
                    it = v.insert(it+1,1);
                    it = v.insert(it+1,1);
                }

    cout<<"Converted vector"<<endl;
    for(int i=0; i<v.size();++i)
        cout<<v[i]<<endl;
}

// пузырек для массива double
void bubblesortD(double* array, int size)
{

    for(int i=0; i<size; ++i)
        for(int j=i+1;j<size; ++j)
            if(array[i]>array[j])
            {
                double temp=array[i];
                array[i]=array[j];
                array[j]=temp;
            }
    cout <<"'[]' sorted:\n";
    for(int i=0; i<10; ++i)
        cout<<array[i]<<endl;
}

//заполнение массива double случайными числами
void fillRandom(double* array, int size)
{
    for(int i=0; i<size ;++i)
    {
        array[i]=2.0 * ( (double)rand() / (double)RAND_MAX ) -1.0;
        cout<<array[i]<<endl;
    }
}

//главная функция по лабораторной
void Lab1_main()
{
    srand(time(NULL));
    std::vector<int> v = std::vector<int>(Arr_Size);
    uint start, finish; //для вычисления времени
    for(int i=0; i<Arr_Size; ++i)
        v[i]=rand();
    start = clock();
    bubblesort(v);
    finish = clock();
    cout<<"Bubblesort time: "<<double(finish-start)/CLOCKS_PER_SEC<<" sec"<<endl;
    start = clock();
    bubblesortIt(v);
    finish = clock();
    cout<<"Iteratorsort time: "<<double(finish-start)/CLOCKS_PER_SEC<<" sec"<<endl;
    start = clock();
    bubblesortAt(v);
    finish = clock();
    cout<<"Atsort time: "<<double(finish-start)/CLOCKS_PER_SEC<<" sec"<<endl;
    start = clock();
    std::sort(v.begin(), v.end());
    finish = clock();
    cout<<"STL time: "<<double(finish-start)/CLOCKS_PER_SEC<<" sec"<<endl;

    //copyData();

    //vectorConverter();

    /*double arr[Arr_Size];
    double arr1[Arr_Size1];
    double arr2[Arr_Size2];
    double arr3[Arr_Size3];
    double arr4[Arr_Size4];

    fillRandom(arr, Arr_Size);
    fillRandom(arr1, Arr_Size1);
    fillRandom(arr2, Arr_Size2);
    fillRandom(arr3, Arr_Size3);
    fillRandom(arr4, Arr_Size4);

    bubblesortD(arr, Arr_Size);
    bubblesortD(arr1, Arr_Size1);
    bubblesortD(arr2, Arr_Size2);
    bubblesortD(arr3, Arr_Size3);
    bubblesortD(arr4, Arr_Size4);*/
}

#endif // LAB1

