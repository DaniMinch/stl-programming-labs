#include <iostream>

template<class T>
struct Coord
{
    T x;
    T y;

    Coord() {}
    Coord(T _x, T _y): x(_x),y(_y){}
};

class Shape_8
{
public:
    Shape_8(int x, int y) : center(x, y) {}
    //virtual ~Shape_8() {}

    bool isMoreLeft(const Shape_8& shape) const
    {
        return center.x < shape.center.x;
    }

    bool isUpper(const Shape_8& shape) const
    {
        return center.y > shape.center.y;
    }

    virtual void draw() const = 0;
protected:
    Coord<int> center;
};

