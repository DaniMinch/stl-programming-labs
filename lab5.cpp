#include "lab5.h"

using namespace std;

void Lab5_main()
{
    vector<DataStruct> vct = createDataStructVector();
    cout<< "Original vector:"<<endl;
    printVector(vct);
    cout << endl << "Sorted vector:"<<endl;
    sort(vct.begin(), vct.end(), sortVector);
    printVector(vct);
    cout << endl << endl;
}

bool sortVector(const DataStruct& struct1, const DataStruct& struct2)
{
    if (struct1.key1 == struct2.key1)
    {
        if (struct1.key2 == struct2.key2)
            return !(struct1.str.length() > struct2.str.length());
        return struct1.key2 < struct2.key2;
    }
    return struct1.key1 < struct2.key1;
}

template<typename T>
void printVector(const vector<T>& vct)
{
    bool first = true;
    cout<<"key1\tkey2\t string"<<endl;
    for (const T& var : vct)
    {
        if (!first)
            cout << "\n";
        else
            first = false;

        cout << var;
    }
    cout<<endl;
}

vector<DataStruct> createDataStructVector(int length)
{
    vector<DataStruct>vct;
    srand(time(NULL));
    mt19937 gen(rand());
    // улучшенное распределение; time берется из chrono
    uniform_int_distribution<int> distrKeys(-5, 5);
    uniform_int_distribution<int> distrStr(0, 10);
    for (int i = 0; i < length; ++i)
        vct.push_back(DataStruct(distrKeys(gen), distrKeys(gen),
                                 strings[distrStr(gen)]));
    return vct;
}
