#ifndef _PHONEBOOK_H_
#define _PHONEBOOK_H_

#include <Record.h>
#include <vector>
#include <memory>

class Phonebook
{
    typedef std::vector<Record*> RecordsList;

    RecordsList m_RecordsList;
    RecordsList::iterator m_CurRecord;

public:
    Phonebook() { }
    ~Phonebook() { }

    RecordsList::iterator begin()
    {
        return m_RecordsList.begin();
    }

    RecordsList::iterator end()
    {
        return m_RecordsList.end();
    }

    RecordsList::const_iterator begin() const
    {
        return m_RecordsList.cbegin();
    }

    RecordsList::const_iterator end() const
    {
        return m_RecordsList.cend();
    }

    Record* current() const
    {
        return *m_CurRecord;
    }

    Record* moveNext()
    {
        return *(++m_CurRecord);
    }

    Record* movePrevious()
    {
        return *(--m_CurRecord);
    }

    Record* move(int movPos)
    {
        if (movPos > 0)
        {
            while (m_CurRecord != (m_RecordsList.end() - 1) && movPos)
            {
                ++m_CurRecord;
                --movPos;
            }
        }
        else if(movPos < 0)
        {
            while (m_CurRecord != m_RecordsList.begin() && movPos)
            {
                --m_CurRecord;
                ++movPos;
            }
        }

        return *m_CurRecord;
    }

    bool isEmpty() const
    {
        return m_RecordsList.empty();
    }

    bool hasNext() const
    {
        return m_CurRecord != m_RecordsList.end() - 1;
    }

    bool hasPrevious() const
    {
        return m_CurRecord != m_RecordsList.begin();
    }

    void insertAfter(Record* record)
    {
        insert(std::inserter(m_RecordsList, m_CurRecord + 1), record);
    }

    void insertBefore(Record* record)
    {
        insert(std::inserter(m_RecordsList, m_CurRecord), record);
    }

    void insertLast(Record* record)
    {
        insert(std::inserter(m_RecordsList, m_RecordsList.end()), record);
    }

    void replaceCurrent(Record* record)
    {
        insert(m_CurRecord, record);;
    }

private:
    template<typename TIterator>
    void insert(TIterator it, Record* record)
    {
        *it = record;
        m_CurRecord = m_RecordsList.begin();
    }
};

#endif //_PHONEBOOK_H_
