#include <QCoreApplication>
#include <lab1.h>
#include <lab2.h>
#include <lab3.h>
#include <lab4.h>
#include <lab5.h>
#include <lab6.h>
#include <lab7.h>
#include <lab8.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    //Lab1_main();
    Lab2_main();
    //Lab3_main();
    //Lab4_main();
    //Lab5_main();
    //Lab6_main();
    //Lab7_main();
    //Lab8_main();

    return a.exec();
}
