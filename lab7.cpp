#include "lab7.h"

using namespace std;
void Lab7_main()
{
    srand(time(NULL));
    vector<int> vct(100);
    generate(vct.begin(), vct.end(),[](){return rand()%1001 - 500;});
    StatisticFunctor stats;
    stats = for_each(vct.begin(), vct.end(), stats);

    cout << "Original Vector:" << endl;
    for_each(vct.begin(), vct.end(), [](const auto& number){cout<<number<<endl;});

    cout << endl << "Statistics of this vector:" << endl;
    cout << stats;
}
