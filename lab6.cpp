#include "lab6.h"

using namespace std;

void Lab6_main()
{
    srand(time(NULL));
    ifstream infile("C:\\Qt\\Projects\\Lab11\\Lab6_test.txt");
    std::string str((istreambuf_iterator<char>(infile)),
                    istreambuf_iterator<char>());
    vector<string> words = parseStringToVector(str);
    cout<<"Original massive:"<<endl;
    for(const auto& s:words)
        cout<<s<<" ";
    cout<<endl;
    showUniqueWords(words);

    vector<Shape> shapes = createVector(10);

    countVertex counter;
    counter = for_each(shapes.begin(),shapes.end(), counter);
    cout<<"Amount of vertex in vector: "<<counter.getCounter()<<endl;

    countShapes(shapes);

    // удаление всех пятиугольников
    shapes.erase(remove_if(shapes.begin(), shapes.end(), isPentagon),shapes.end());

    vector<Point> points = createPointVectorfromShapes(shapes);

    // сортировка
    sort(shapes.begin(), shapes.end(),sortShapes);

    // вывод векторов
    cout<<endl<<endl<<"Random points are:"<<endl;
    for_each(points.begin(),points.end(),printPoint);
    cout<<endl<<endl;
    for_each(shapes.begin(),shapes.end(),printShape);
}

// разделяет строку на подстроки, заносящиеся в вектор; слова отделены пробелами,
// табами, EOL'ом и EOF'ом.
vector<string> parseStringToVector(string & str)
{
    vector<string> v;
    size_t found = str.find_first_of(" \n\t");
    while(found != string::npos)
    {
        if (isalnum(str[found-1]))
            v.push_back(str.substr(found-1,1));
        found = str.find_first_of(" \n\t", found+1);
    }
    v.push_back(str.substr(str.find_last_of(" \n\t")+1,
                           str.length()-str.find_last_of(" \n\t")+1));
    return v;
}

// функция находит все уникальные элементы вектора
void showUniqueWords(vector<string> &v)
{
    sort(v.begin(),v.end()); // unique удаляет только стоящие рядом элементы
    v.erase(unique(v.begin(),v.end()),v.end());
    cout<<"Unique numbers:"<<endl;
    for(const auto& s:v)
        cout<<s<<" ";
    cout<<endl;
}

//-----------------------------------------------------------------------------

// конструктор фигуры, координаты задаются случайно в диапазоне от -100 до 100
// от -400 до 400 для прямоугольников
Shape::Shape(int _num, bool _isSquare = false)
    :vertex_num(_num), square(_isSquare)
{
    Point pnt;
    if(vertex_num !=4)
        for(int i = 0; i < vertex_num; ++i)
        {
            pnt.x = (rand()%2001-1000)/10;
            pnt.y = (rand()%2001-1000)/10;
            vertexes.push_back(pnt);
        }
    else
    {
        Point pnt1, pnt2, pnt3;
        double vx,vy;
        pnt.x = (rand()%2001-1000)/10;
        pnt.y = (rand()%2001-1000)/10;
        vertexes.push_back(pnt);
        pnt1.x = (rand()%2001-1000)/10;
        pnt1.y = (rand()%2001-1000)/10;
        vertexes.push_back(pnt1);
        if(_isSquare == true)
        {
            vx = pnt1.x - pnt.x;
            vy = pnt1.y - pnt.y;
        }
        else
        {
            // случайный коэфициент от 0.01 до 4.01 для второй стороны
            double random = 4.0 * ( (double)rand() / (double)RAND_MAX) + 0.01;
            vx = (pnt1.x - pnt.x)*random;
            vy = (pnt1.y - pnt.y)*random;
        }
        // получаем нижние точки, поворотом имеющегося вектора на 90 градусов
        pnt2.x = pnt.x + vy;
        pnt2.y = pnt.y - vx;
        vertexes.push_back(pnt2);
        pnt3.x = pnt2.x + vy;
        pnt3.y = pnt2.x - vx;
        vertexes.push_back(pnt3);
    }

}

// создание случайной фигуры, только выбор фигуры, координаты - в конструкторе
Shape createRandomShape()
{
    int random;
    random = rand()%4;
    switch (random)
    {
    case 0:                         // треугольник
        return Shape(3);
        break;
    case 1:                         // прямоугольник
        return Shape(4);
        break;
    case 2:                         // квадрат
        return Shape(4, true);
        break;
    case 3:                         // пятиугольник
        return Shape(5);
        break;
    default:
        return Shape();
        cout<<"Undefined Behavior: random generator failure!";
        break;
    }
}

// создание вектора
vector<Shape> createVector(int length)
{
    vector<Shape> v(length);
    generate(v.begin(), v.end(), createRandomShape);
    return v;
}

// функция определения типа фигуры, нужны для подсчета фигур
bool isTriangle(const Shape& sh){ return (sh.vertex_num==3);}
bool isSquare(const Shape& sh){ return ((sh.vertex_num==4)&&(sh.square));}
bool isRectangle(const Shape& sh){ return ((sh.vertex_num==4)&&(!sh.square));}
bool isPentagon(const Shape& sh){ return (sh.vertex_num==5);}

// подсчет фигур
void countShapes(const vector<Shape> & v)
{
    cout<<"Amount of triangles: "<<count_if(v.begin(),v.end(),isTriangle)<<endl;
    cout<<"Amount of squares: "<<count_if(v.begin(),v.end(),isSquare)<<endl;
    cout<<"Amount of rectangles: "<<count_if(v.begin(),v.end(),isRectangle)<<endl;
    cout<<"Amount of pentagons: "<<count_if(v.begin(),v.end(),isPentagon)<<endl;
}

// создает вектор точек из вектора фигур
vector<Point> createPointVectorfromShapes(const vector<Shape> & vct)
{
    vector<Point> vPoint(vct.size());
    transform(vct.begin(),vct.end(),vPoint.begin(),getRandomPoint);
    return vPoint;
}

// получает одну из вершин фигуры, принимаемой параметром
Point getRandomPoint(const Shape &sh)
{
    return sh.vertexes[rand()%sh.vertexes.size()];
}

// сортировка по количеству элементов, квадраты раньше прямоугольников
bool sortShapes(Shape sh1, Shape sh2)
{
    bool temp;
    if (sh1.vertex_num == sh2.vertex_num)
        temp = sh1.square;
    else
        temp = (sh1.vertex_num < sh2.vertex_num);
    cout<<temp<<" "<<sh1.vertex_num<<" "<<sh2.vertex_num<<endl;
    return temp;
}

// вывод фигуры
void printShape(Shape sh)
{
    cout<<"Figure ";
    switch (sh.vertex_num) {
    case 3:
        cout<<"triangle ";
        break;
    case 4:
        if(sh.square)
            cout<<"square ";
        else
            cout<<"rectangle ";
        break;
    case 5:
        cout<<"pentagon ";
        break;
    default:
        cout<<"is undefined!";
        break;
    }
    cout<<"has "<<sh.vertex_num<<" vertexes. Their coordinates are: "<<endl;
    for_each(sh.vertexes.begin(), sh.vertexes.end(), printPoint);
    cout<<endl<<endl;
}

// вывод координат точки
void printPoint(Point v)
{
    cout<<"("<<v.x<<";"<<v.y<<");\t";
}
