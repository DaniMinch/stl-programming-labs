#include <lab8.h>

using namespace std;

void Lab8_main()
{
    list<double> lstD(10);
    srand(time(NULL));
    generate(lstD.begin(), lstD.end(),[](){return ( (double)rand() / (double)RAND_MAX )*10;});
    cout<<"Original:"<<endl;
    for_each(lstD.begin(),lstD.end(),[](const auto& dbl){cout<<dbl<<endl;});
    // bind1st применяет ко всем элементам значение M_PI
    transform(lstD.begin(), lstD.end(), lstD.begin(),
                   bind1st(multiplies<double>(),M_PI));
    cout<<"Multiplied by PI:"<<endl;
    for_each(lstD.begin(),lstD.end(),[](const auto& dbl){cout<<dbl<<endl;});

    list<Shape_8*> shapesList;
    shapesList.push_back(new Circle(0, 7));
    shapesList.push_back(new Square(2, 6));
    shapesList.push_back(new Triangle(-1, -9));
    shapesList.push_back(new Triangle(-2, 2));

    function<bool (Shape_8*, Shape_8*)> isMoreRight =
                [](Shape_8* sh1, Shape_8* sh2)->bool{return !IsMoreLeftPredicate()(sh1, sh2); };

    cout << endl << "Original list:" << endl;
    drawShapes(shapesList);
    cout << endl;

    shapesList.sort(IsMoreLeftPredicate());

    cout << "SortedLeftToRight:" << endl;
    drawShapes(shapesList);
    cout << endl;

    shapesList.sort(isMoreRight);

    cout << "SortedRightToLeft" << endl;
    drawShapes(shapesList);
    cout << endl;

    shapesList.sort(IsUpperPredicate());

    cout << "SortedUptoDown" << endl;
    drawShapes(shapesList);
    cout << endl;

    shapesList.sort(binary_negate<IsUpperPredicate>(IsUpperPredicate()));

    cout << "SortedDownToUp" << endl;
    drawShapes(shapesList);
    cout << endl;
}

void drawShapes(const list<Shape_8 *>& lst)
{
    for_each(lst.begin(), lst.end(), [](const auto& shape)
    {
        shape->draw();
        cout << endl;
    });
}
