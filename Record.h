#ifndef _RECORD_H_
#define _RECORD_H_

#include <iostream>
#include <string>

class Record
{
    std::string name;
    std::string phone;

public:
    Record(const char* name, const char* phone) : name(name), phone(phone) { }
    Record(const std::string& name, const std::string& phone) : name(name), phone(phone) { }
    Record(const Record& record) : name(record.name), phone(record.phone) { }
    //~Record() { }

    Record& operator=(const Record& record)
    {
        name = record.name;
        phone = record.phone;

        return *this;
    }

    const std::string& getName() const
    {
        return name;
    }

    void setName(const std::string& _name)
    {
        name = _name;
    }

    const std::string& getPhone() const
    {
        return phone;
    }

    void setPhone(const std::string& _phone)
    {
        phone = _phone;
    }

    friend std::ostream& operator<<(std::ostream& os, const Record& rec)
    {
        os << "Name: " << rec.name << "\tPhone: " << rec.phone;
        return os;
    }
};


#endif //_RECORD_H_
