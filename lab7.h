/*
Необходимо выполнить следующее задание:

1.	Разработать функтор,  позволяющий собирать статистику о последовательности
целых чисел (послед может содержать числа от -500 до 500). Функтор после
обработки последовательности алгоритмом for_each должен предоставлять следующую
статистику:
a.	Максимальное число в последовательности
b.	Минимальное число в последовательности
c.	Среднее чисел в последовательности
d.	Количество положительных чисел
e.	Количество отрицательных чисел
f.	Сумму нечетных элементов последовательности
g.	Сумму четных элементов последовательности
h.	Совпадают ли первый и последний элементы последовательности.

Проверить работу программы на случайно сгенерированных последовательностях.


*/
#ifndef LAB7_H
#define LAB7_H

#include <iostream>
#include <vector>
#include <chrono>
#include <functional>
#include <algorithm>

struct StatisticFunctor : std::unary_function<int, void>
{
    int max = int();
    int min = int();
    double avg = double();
    unsigned positiveCount = 0;
    unsigned negativeCount = 0;
    unsigned overallCount = 0;
    int oddSum = int();
    int evenSum = int();
    bool isEqualLastFirst = bool();
    int first = int();

    StatisticFunctor() { }

    void operator()(int value)
    {
        if (!overallCount) first = value;
        isEqualLastFirst = (first==value);
        if (value > max) max = value;
        if (value < min) min = value;
        if (value > 0) ++positiveCount;
        if (value < 0) ++negativeCount;
        ++overallCount;

        if (!value %2)
            oddSum += value;
        else
            evenSum += value;

        avg = double(evenSum + oddSum) / double(overallCount);
    }

    friend std::ostream& operator<<(std::ostream& os,
                               const StatisticFunctor& func)
    {
        os << "Minimal Value: " << func.min << std::endl <<
              "Maximal Value: " << func.max << std::endl <<
              "Average Velue: " << func.avg << std::endl <<
              "Count of positive numbers: " << func.positiveCount << std::endl <<
              "Count of negative numbers: " << func.negativeCount << std::endl <<
              "Sum of even numbers: " << func.evenSum << std::endl <<
              "Sum of odd numbers: " << func.oddSum << std::endl <<
              "Last and first elements are ";
        if (func.isEqualLastFirst)
            os<<"equal."<<std::endl;
        else
            os<<"unequal."<<std::endl;
        return os;
    }
};

void Lab7_main();

#endif // LAB7_H
